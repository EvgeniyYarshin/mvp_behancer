package com.elegion.test.behancer.common;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.elegion.test.behancer.R;

import moxy.MvpAppCompatFragment;

public abstract class PresenterFragment extends MvpAppCompatFragment {

    protected abstract BasePresenter getPresenter();

    @Override
    public void onDetach() {
        if(getPresenter() != null)
            getPresenter().disposeAll();

        super.onDetach();
    }

    /*protected View inflateFragment(int resId, LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(resId, container, false);
        //FrameLayout layout = view.findViewById(R.id.fragmentContainer);
        //setHasOptionsMenu(true);
        return view;
    }*/
}
