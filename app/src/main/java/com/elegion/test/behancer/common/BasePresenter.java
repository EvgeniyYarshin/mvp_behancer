package com.elegion.test.behancer.common;

import io.reactivex.disposables.CompositeDisposable;
import moxy.MvpPresenter;

public abstract class BasePresenter<View extends BaseView> extends MvpPresenter<View> {

    protected CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    void disposeAll() {
        mCompositeDisposable.clear();
    }
}
