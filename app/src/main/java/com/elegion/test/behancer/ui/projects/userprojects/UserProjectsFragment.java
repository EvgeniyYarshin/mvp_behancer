package com.elegion.test.behancer.ui.projects.userprojects;

import android.os.Bundle;

import com.elegion.test.behancer.ui.projects.ListFragment;
import com.elegion.test.behancer.ui.projects.ProjectsPresenter;

import moxy.presenter.InjectPresenter;
import moxy.presenter.ProvidePresenter;

import static com.elegion.test.behancer.ui.profile.ProfileFragment.PROFILE_KEY;

public class UserProjectsFragment extends ListFragment {

    @InjectPresenter
    ProjectsPresenter mPresenter;

    @ProvidePresenter
    ProjectsPresenter providePresenter() {
        return new ProjectsPresenter(mStorage);
    }

    protected ProjectsPresenter getPresenter() {
        return mPresenter;
    }

    static UserProjectsFragment newInstance(Bundle args) {
        UserProjectsFragment fragment = new UserProjectsFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onRefreshData() {
        assert getArguments() != null;
        mPresenter.getUserProjects(getArguments().getString(PROFILE_KEY));
    }

}
