package com.elegion.test.behancer.ui.projects.projects;

import com.elegion.test.behancer.ui.projects.ListFragment;
import com.elegion.test.behancer.ui.projects.ProjectsPresenter;

import moxy.presenter.InjectPresenter;
import moxy.presenter.ProvidePresenter;

public class ProjectsFragment extends ListFragment {

    @InjectPresenter
    ProjectsPresenter mPresenter;

    @ProvidePresenter
    ProjectsPresenter providePresenter() {
        return new ProjectsPresenter(mStorage);
    }

    protected ProjectsPresenter getPresenter() {
        return mPresenter;
    }

    static ProjectsFragment newInstance() {
        return new ProjectsFragment();
    }

    @Override
    public void onItemClick(String username) {
        mPresenter.openProfileFragment(username);
    }

    @Override
    public void onRefreshData() {
        mPresenter.getProjects();
    }
}
