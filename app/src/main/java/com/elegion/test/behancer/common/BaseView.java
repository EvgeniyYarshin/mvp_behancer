package com.elegion.test.behancer.common;


import moxy.MvpView;

public interface BaseView extends MvpView {

    void showRefresh();

    void hideRefresh();

    void showError();

}
