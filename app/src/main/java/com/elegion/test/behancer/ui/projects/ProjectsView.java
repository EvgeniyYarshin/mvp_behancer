package com.elegion.test.behancer.ui.projects;

import androidx.annotation.NonNull;

import com.elegion.test.behancer.common.BaseView;
import com.elegion.test.behancer.data.model.project.Project;

import java.util.List;

public interface ProjectsView extends BaseView {

    void showProjects(@NonNull List<Project> projects);

    void openProfileFragment(@NonNull String username);
}
